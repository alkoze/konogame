
console.log("asd")

import { Logic } from './logic'
import { Ai } from './ai'

let brain = new Logic()
let aii = new Ai()
let box = document.getElementById('box')!
let boxAi = document.getElementById('boxAi')!
let boxAiVsAi = document.getElementById('boxAiVsAi')!
let checkboxColor = document.querySelector("input[name=checkboxColor]")! as HTMLElement;
let checkboxAi = document.querySelector("input[name=checkboxAi]")! as HTMLElement;
let checkboxAiVsAi = document.querySelector("input[name=checkboxAiVsAi]")! as HTMLElement;
let startButton = document.getElementById('startB')! as HTMLElement
let restsrtB = document.getElementById("restartB")! as HTMLElement;
let playerWin1: HTMLElement = document.getElementById("hiddenPlayer1W")!;
let playerWin2: HTMLElement = document.getElementById("hiddenPlayer2W")!;

let dis = document.getElementById("js-display") as HTMLElement;
let nav = document.getElementById("js-nav") as HTMLElement;
let zxc = document.getElementById("body") as HTMLElement;

let body = document.getElementById("gamefield");
let HTMLrow = document.createElement("tr")

let green = document.createElement("td") as HTMLElement;
green.innerHTML = "<a class=\"center 00897b teal darken-1  waves-effect waves-light btn js-btn\"></a>";
let orange = document.createElement("td") as HTMLElement;
orange.innerHTML = "<a class=\"waves-effect waves-light btn #e64a19 deep-orange darken-2  js-btn\"></a>";
let grey = document.createElement("td") as HTMLElement;
grey.innerHTML = "<a class=\"waves-effect waves-light btn grey js-btn\"></a>";

let boxValue: string = 'green';
let index: number = 0;
let pl1: string = '1'
let pl2: string = '2'
let cpl: string = pl1
let arr: Array<HTMLElement> = []
let started: boolean = false
let vsAi: boolean = false
let aiVsAi: boolean = false
interface IDict {
    [key: number]: string;
    0: string;
    1: string;
    4: string;
    3: string;
    2: string;
}
let rowDict: IDict = { 0: '4', 1: '3', 4: '0', 3: '1', 2: '2' }

index = 0;

checkboxColor.addEventListener('change', function (this: any) {
    if (this.checked) {
        cpl = pl2
        boxValue = 'orange'
        dis.style.background = "#F6DDCC"
        nav.style.background = "#D35400 "
        startButton.className = 'waves-effect waves-orange btn z-depth-0'
        restsrtB.className = 'waves-effect waves-orange btn z-depth-0'
    } else {
        cpl = pl1
        boxValue = 'green'
        dis.style.background = "#D1F2EB"
        nav.style.background = "#1ABC9C";
        startButton.className = 'waves-effect waves-green btn z-depth-0'
        restsrtB.className = 'waves-effect waves-green btn z-depth-0'

    }
});
checkboxAi.addEventListener('change', function (this: any) {
    if (this.checked) {
        vsAi = true
        aiVsAi = false
    } else {
        vsAi = false
    }
});
checkboxAiVsAi.addEventListener('change', function (this: any) {
    if (this.checked) {
        aiVsAi = true
        vsAi = true
    } else {
        aiVsAi = false
    }
});


function startGame(this: any) {
    setTimeout(() => {
        brain.board = [
            [1, 1, 1, 1, 1],
            [1, 0, 0, 0, 1],
            [0, 0, 0, 0, 0],
            [2, 0, 0, 0, 2],
            [2, 2, 2, 2, 2]
        ]
        let buttons = body!.querySelectorAll('.js-btn') as NodeListOf<HTMLElement>;

        for (const button of buttons) {
            (button as HTMLAnchorElement).addEventListener('click', pressed)
        }
        started = true
        restsrtB.style.display = 'none'
        startButton.style.display = "none"
        box.style.display = "none"
        boxAi.style.display = "none"
        boxAiVsAi.style.display = "none"
        playerWin1.style.display = 'none'
        playerWin2.style.display = 'none'
        drawmap()
    }, 500);
}

function drawmap() {
    body!.innerHTML = "";
    let rowiterator = 0;
    for (const row of brain.board) {
        let coliterator = 0
        for (const val of row) {
            if (grey.lastChild! instanceof HTMLAnchorElement && green.lastChild! instanceof HTMLAnchorElement && orange.lastChild! instanceof HTMLAnchorElement) {
                if (val == 0) {
                    grey.lastChild!.dataset.row = String(rowiterator);
                    grey.lastChild!.dataset.col = String(coliterator);
                    grey.lastChild!.dataset.val = String(val);
                    HTMLrow.append(grey.cloneNode(true))
                }
                if (val == 1) {
                    green.lastChild!.dataset.row = String(rowiterator);
                    green.lastChild!.dataset.col = String(coliterator);
                    green.lastChild!.dataset.val = String(val);
                    HTMLrow.append(green.cloneNode(true))
                }
                if (val == 2) {
                    orange.lastChild!.dataset.row = String(rowiterator);
                    orange.lastChild!.dataset.col = String(coliterator);
                    orange.lastChild!.dataset.val = String(val);
                    HTMLrow.append(orange.cloneNode(true));
                }
                coliterator++;
                if (coliterator == 5) {
                    rowiterator++;
                    body!.append(HTMLrow.cloneNode(true));
                    HTMLrow.innerHTML = "";
                }
            }
        }
    }
    if (started) {
        let buttons = body!.querySelectorAll('.js-btn') as NodeListOf<HTMLElement>;
        for (const button of buttons) {
            (button as HTMLAnchorElement).addEventListener('click', pressed)
        }
        if (vsAi) {
            if (cpl == pl2) {
                aii.getMoves(brain.board, '2')
                let move: HTMLElement[] = aii.getMove()
                console.log("asdasdasd");
                brain.makemove2(move)
                aii.avaliableMoves = []
                changeColor()
                if (brain.checkwin2(buttons)  === false) {

                    setTimeout(() => {

                        drawmap()
                    }, 500);
                }
            }
            else{
                if (aiVsAi) {
                    if (cpl == pl1) {
                        aii.getMoves(brain.board.slice().reverse(), '1')
                        let move: HTMLElement[] = aii.getMove()
    
                        move[0].dataset.row = rowDict[Number(move[0].dataset.row!)]
                        move[1].dataset.row = rowDict[Number(move[1].dataset.row!)]
                        brain.makemove2(move)
                        aii.avaliableMoves = []
                        changeColor()
                        if (brain.checkwin2(buttons) === false) {
    
                            setTimeout(() => {
    
                                drawmap()
                            }, 500);
                        }
                    }
                }
            }
        }
        if (brain.checkwin2(buttons) !== false) {
            showWin(brain.checkwin2(buttons))
        }
    }
}
drawmap();

startButton.addEventListener('click', startGame)

function pressed(this: any, event: MouseEvent) {
    if (arr.length == 1 && this.dataset.val == cpl) {
        arr.pop()
    }
    if ((arr.length == 0 && this.dataset.val == cpl) || (arr.length == 1 && this.dataset.val == '0')) {
        arr.push(this)
    }
    if (arr.length == 2) {
        console.log(brain.board);
        console.log(brain.board.slice().reverse());
        if (brain.checkMove2(arr)) {
            brain.makemove2(arr)
            changeColor()
            drawmap()
        }
        arr = []

    }
}

function showWin(condition: any) {
    if (condition == 'Green') {
        dis.style.background = "#D1F2EB";
        nav.style.background = "#1ABC9C";
        let x: HTMLElement = document.getElementById("hiddenPlayer1W")!;
        if (x.style.display == "none") {
            x.style.display = "block";
        } else {
            x.style.display = "none";
        }
    } else if (condition == 'Orange') {
        dis.style.background = "#F6DDCC";
        nav.style.background = "#D35400";
        let x: HTMLElement = document.getElementById("hiddenPlayer2W")!;
        if (x.style.display == "none") {
            x.style.display = "block";
        } else {
            x.style.display = "none";
        }
    }
    box.style.display = "block"
    boxAi.style.display = "block"
    boxAiVsAi.style.display = "block"
    let buttons = body!.querySelectorAll('.js-btn') as NodeListOf<HTMLElement>;

    cpl = pl1
    checkboxColor
    for (const button of buttons) {
        button.removeEventListener('click', pressed)
    }
    restsrtB.style.display = "inline-table"
}

function changeColor() {
    if (cpl == pl1) {
        cpl = pl2
        dis.style.background = "#F6DDCC";
        nav.style.background = "#D35400";
        boxValue = 'green'
    } else {
        boxValue = 'orange'
        cpl = pl1
        dis.style.background = "#D1F2EB";
        nav.style.background = "#1ABC9C";
    }
}

restsrtB.addEventListener('click', startGame)
