

// var boxValu = require('./index')
export class Logic {
    public board : number[][]
    constructor() {
        this.board =[
            [1,1,1,1,1],
            [1,0,0,0,1],
            [0,0,0,0,0],
            [2,0,0,0,2],
            [2,2,2,2,2]
        ]
    }
    getPosition(elem: any) {
        let dims = { offsetLeft: 0, offsetTop: 0 };
        do {
            dims.offsetLeft += elem.offsetLeft;
            dims.offsetTop += elem.offsetTop;
        }
        while (elem = elem.offsetParent);
        return dims;
    }
    checkMove2(arr: Array<HTMLElement>) {
        return (Math.abs(Number(arr[0].dataset.row) - Number(arr[1].dataset.row)) == 1 && Math.abs(Number(arr[0].dataset.col) - Number(arr[1].dataset.col)) == 1 && arr[1].dataset.val == '0')
    }
    makemove2(arr: Array<HTMLElement>){
        // let roww :number = Number(arr[0].dataset.row)
        // let coll :number = Number(arr[0].dataset.col)
        // let val :number = Number(arr[1].dataset.val)
        this.board[Number(arr[0].dataset.row)][Number(arr[0].dataset.col)] = Number(Number(arr[1].dataset.val))
        // roww = Number(arr[1].dataset.row)
        // coll = Number(arr[1].dataset.col)
        // val = Number(arr[0].dataset.val)
        this.board[Number(arr[1].dataset.row)][Number(arr[1].dataset.col)] = Number(Number(arr[0].dataset.val))
    }
     checkwin2(arr: NodeListOf<HTMLElement>){
        let p1ind = 0
        let p2ind = 0
        for (const btn of arr) {
            if(btn.dataset.row == '0' && btn.dataset.val == '2'){
                p2ind++
            }
            if (btn.dataset.row == '1' && (btn.dataset.col == '0' || btn.dataset.col == '4') && btn.dataset.val == '2' ) {
                p2ind++
            }
            if(btn.dataset.row == '4' && btn.dataset.val == '1'){
                p1ind++
            }
            if (btn.dataset.row == '3' && (btn.dataset.col == '0' || btn.dataset.col == '4') && btn.dataset.val == '1') {
                p1ind++
            }
        }
        if (p1ind == 7){
            return "Green"
        }
        if (p2ind == 7){
            return "Orange"
        }
        return (false)     
     }
}
