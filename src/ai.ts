export class Ai {

    private arr: Array<HTMLElement> = []
    public avaliableMoves: Array<Array<HTMLElement>> = [];
    public board: number[][] = []
    public asd:HTMLElement[]=[]
    public previousmove = {'1': [] as HTMLElement[], '2': [] as HTMLElement[]}
    public currentPlayerVal:string = '2'

    getMoves(board: number[][], playerVal: string) {
        this.currentPlayerVal = playerVal
        this.board = board

        let rowInd = 0;
        let colInd = 0;
        for (const row of board) {
            colInd = 0
            for (const cell of row) {
                if (cell == Number(this.currentPlayerVal)) {
                    // this.baseVertex.push(new Vertex(null, colInd, rowInd))
                    // if (board[rowInd + 1][colInd + 1] == 0){

                    // }
                    if (rowInd == 4) {
                        if (colInd != 0) {
                            if (board[rowInd - 1][colInd - 1] == 0) {
                                this.pushAll(rowInd, colInd, rowInd - 1, colInd - 1);
                            }
                        }
                        if (colInd != 4) {

                            if (board[rowInd - 1][colInd + 1] == 0) {
                                this.pushAll(rowInd, colInd, rowInd - 1, colInd + 1);
                            }
                        }
                    }
                    else if (rowInd == 0) {
                        if (colInd != 0) {

                            if (board[rowInd + 1][colInd - 1] == 0) {
                                this.pushAll(rowInd, colInd, rowInd + 1, colInd - 1);
                            }
                        }
                        if (colInd != 4) {
                            if (board[rowInd + 1][colInd + 1] == 0) {
                                this.pushAll(rowInd, colInd, rowInd + 1, colInd + 1);
                            }
                        }
                    } else {
                        if (colInd != 0) {
                            if (board[rowInd - 1][colInd - 1] == 0) {
                                this.pushAll(rowInd, colInd, rowInd - 1, colInd - 1);
                            }
                            if (board[rowInd + 1][colInd - 1] == 0) {
                                this.pushAll(rowInd, colInd, rowInd + 1, colInd - 1);
                            }
                        }
                        if (colInd != 4) {
                            if (board[rowInd + 1][colInd + 1] == 0) {
                                this.pushAll(rowInd, colInd, rowInd + 1, colInd + 1);
                            }
                            if (board[rowInd - 1][colInd + 1] == 0) {
                                this.pushAll(rowInd, colInd, rowInd - 1, colInd + 1);
                            }
                        }
                    }
                }
                colInd++;
            }
            rowInd++;
        }

    }

    getMove(): Array<HTMLElement> {
        let filtered = this.avaliableMoves.filter(move => {
            if (Number(move[0].dataset.row!) > Number(move[1].dataset.row!)) {
                return move
            }
        })
        let midFilter = filtered.filter(move => {
            if (Number(move[0].dataset.row!) == 2) {
                if ((this.board[0][(Number(move[1].dataset.col) + 1)] == 0 || this.board[0][(Number(move[1].dataset.col) - 1)] == 0)) {
                    return move
                }
            }
        })
        let enndFiltered = filtered.filter(move => {
            if (Number(move[1].dataset.row!) == 0 || (Number(move[1].dataset.row!) == 1 && (Number(move[1].dataset.col!) == 0 || Number(move[1].dataset.col!) == 4))) {
                return move
            }
        })
        let noMoreMoves = this.avaliableMoves.filter(move => {
            if (!(Number(move[0].dataset.row) === 1 && (Number(move[0].dataset.col) === 0 || Number(move[0].dataset.col) === 4)) || !(Number(move[0].dataset.row) === 0)) {
                return move
            }
        })

        if (noMoreMoves.length > 0) {
            this.avaliableMoves = noMoreMoves
        }
        if (filtered.length > 0 && noMoreMoves.length > 2) {
            this.avaliableMoves = filtered;
        }
        if (midFilter.length > 0) {
            this.avaliableMoves = midFilter;
        }
        if (enndFiltered.length > 0) {
            this.avaliableMoves = enndFiltered;
        }
        if (this.avaliableMoves.length > 1) {
            if (this.currentPlayerVal === '1'|| this.currentPlayerVal === '2') {
            if (this.previousmove[this.currentPlayerVal].length > 0) {
                    let prevMove =this.previousmove[this.currentPlayerVal]
                    let noPrevMoves = this.avaliableMoves.filter(move => {
                        if (!(Number(prevMove[0].dataset.col) === Number(move[1].dataset.col) 
                        && Number(prevMove[0].dataset.row) === Number(move[1].dataset.row))) {
                            return move
                        }
                    })
                    if (noPrevMoves.length > 0) {
                        this.avaliableMoves = noPrevMoves
                    }
                }
            }
        }
        let index = Math.floor(Math.random() * this.avaliableMoves.length)

        if (this.currentPlayerVal === '1' || this.currentPlayerVal === '2') {
            this.previousmove[this.currentPlayerVal] = this.avaliableMoves[index]
        }
        return this.avaliableMoves[index];
    }

    private pushAll(rowInd: number, colInd: number, rowInd2: number, colInd2: number) {
        let color = document.createElement("a") as HTMLElement;
        let grey = document.createElement("a") as HTMLElement;
        color.dataset.row = rowInd.toString();
        color.dataset.col = colInd.toString();
        grey.dataset.row = rowInd2.toString();
        grey.dataset.col = colInd2.toString();
        color.dataset.val = this.currentPlayerVal
        grey.dataset.val = '0'
        this.arr.push(color);
        this.arr.push(grey);
        this.avaliableMoves.push(this.arr);
        this.arr = []
    }
}
